#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from baa.data_classes.user import User


if __name__ == '__main__':

    user = User('my_user')
    user.fullname = 'My User'

    check_account = user.new_account('Check Account', '12-3456-7890123-45')
    check_account.balance = 2000
    check_account.new_transaction(2000, '23-4567-8901234-56', '2018.10.05 09:32:30', 'Pay')
    tran = check_account.new_transaction(-500, '34-5678-9012345-67', '2018.10.09 15:32:30', 'Rent')

    print(tran)
    print(check_account.balance)

    gst_cat = check_account.new_category('GST')
    gst_cat.new_virtual_transaction(300, '2018.10.11 19:32:30', 'GST')
    gst_cat.new_virtual_transaction(50, '2018.10.12 23:32:30', 'OT GST')

    savings_cat = check_account.new_category('Savings')
    sav = savings_cat.new_virtual_transaction(amount=100, description='Savings')

    print(check_account.sum_categories)
    print(check_account.sum_uncategorised)

    print(sav)

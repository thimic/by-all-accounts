#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from baa.data_classes.transaction import Transaction, VirtualTransaction


class Account(object):

    def __init__(self, name, number):
        self._name = name
        self._number = number
        self._transactions = []
        self._base_balance = 0
        self._categories = []

    def __repr__(self):
        r = (
            f'{self.__class__.__name__}('
            f'name={self.name!r}, balance={self.balance!r}'
            f')'
        )
        return r

    @property
    def name(self):
        return self._name

    @property
    def number(self):
        return self._number

    @property
    def transactions(self):
        return self._transactions

    @property
    def base_balance(self):
        return self._base_balance

    @base_balance.setter
    def base_balance(self, value):
        self._base_balance = value

    @property
    def categories(self):
        return self._categories

    @property
    def balance(self):
        current_balance = sum(
            [t.amount for t in self.transactions] + [self.base_balance]
        )
        return current_balance

    @balance.setter
    def balance(self, value):
        diff = value - self.balance
        self.base_balance += diff

    @property
    def sum_categories(self):
        return sum([va.balance for va in self.categories])

    @property
    def sum_uncategorised(self):
        return self.balance - self.sum_categories

    def new_category(self, name):
        category = Category(name=name, parent=self)
        self.categories.append(category)
        return category

    def new_transaction(self, amount, account, timestamp, description=None):
        transaction = Transaction(
            amount=amount,
            account=account,
            timestamp=timestamp,
            description=description
        )
        self.transactions.append(transaction)
        return transaction


class Category(object):

    def __init__(self, name, parent=None):
        self._name = name
        self._parent = parent
        self._virtual_transactions = []
        self._base_balance = 0

    def __repr__(self):
        r = (
            f'{self.__class__.__name__}('
            f'name={self.name!r}, balance={self.balance!r}'
            f')'
        )
        return r

    @property
    def name(self):
        return self._name

    @property
    def parent(self):
        return self._parent

    @property
    def virtual_transactions(self):
        return self._virtual_transactions

    @property
    def base_balance(self):
        return self._base_balance

    @base_balance.setter
    def base_balance(self, value):
        self._base_balance = value

    @property
    def balance(self):
        current_balance = sum(
            [t.amount for t in self.virtual_transactions] + [self.base_balance]
        )
        return current_balance

    @balance.setter
    def balance(self, value):
        diff = value - self.balance
        self.base_balance += diff

    def new_virtual_transaction(self, amount, timestamp=None, description=None):
        if amount + self.parent.sum_categories > self.parent.balance:
            raise ValueError('Insufficient funds in parent account.')
        virtual_transaction = VirtualTransaction(
            amount=amount, timestamp=timestamp, description=description
        )
        self.virtual_transactions.append(virtual_transaction)
        return virtual_transaction

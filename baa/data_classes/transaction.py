#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime


class Transaction(object):

    def __init__(self, amount, account,  timestamp, description=None):
        self._amount = amount
        self._account = account
        if isinstance(timestamp, str):
            timestamp = datetime.strptime(timestamp, '%Y.%m.%d %H:%M:%S')
        self._timestamp = timestamp
        self._description = description

    def __repr__(self):
        r = (
            f'{self.__class__.__name__}('
            f'amount={self.amount}, '
            f'account={self.account!r}, '
            f'description={self.description!r}, '
            f'timestamp=\'{self.timestamp:%Y.%m.%d %H:%M:%S}\''
            f')'
        )
        return r

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        self._amount = value

    @property
    def account(self):
        return self._account

    @account.setter
    def account(self, value):
        self._account = value

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        if isinstance(value, str):
            value = datetime.strptime(value, '%Y.%m.%d %H:%M:%S')
        self._timestamp = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value


class VirtualTransaction(object):

    def __init__(self, amount, timestamp=None, description=None):
        self._amount = amount
        if isinstance(timestamp, str):
            timestamp = datetime.strptime(timestamp, '%Y.%m.%d %H:%M:%S')
        self._timestamp = timestamp or datetime.now()
        self._description = description

    def __repr__(self):
        r = (
            f'{self.__class__.__name__}('
            f'amount={self.amount}, '
            f'description={self.description!r}, '
            f'timestamp=\'{self.timestamp:%Y.%m.%d %H:%M:%S}\''
            f')'
        )
        return r

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        self._amount = value

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        if isinstance(value, str):
            value = datetime.strptime(value, '%Y.%m.%d %H:%M:%S')
        self._timestamp = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

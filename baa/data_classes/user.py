#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from baa.data_classes.account import Account


class User(object):

    def __init__(self, username=None):
        self._accounts = {}
        self._fullname = None
        self._username = username

    @property
    def accounts(self):
        return self._accounts

    @property
    def fullname(self):
        return self._fullname

    @fullname.setter
    def fullname(self, value):
        self._fullname = value

    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, value):
        self._username = value

    def new_account(self, name, number):
        account = Account(name=name, number=number)
        self.accounts[name] = account
        return account
